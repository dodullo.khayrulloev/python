from typing import Optional

from sqlalchemy import Column
from src.app.value_object.sqlalchemy.id_decorator import IdDecorator
from src.app.value_object.sqlalchemy.name_decorator import NameDecorator
from src.app.value_object.sqlalchemy.description_decorator import DescriptionDecorator
from src.app.value_object.id_value_object import Id
from src.app.value_object.name_value_object import Name
from src.app.value_object.description_value_object import Description
from src.config.db import Base


class User(Base):
    __tablename__ = 'user'
    id = Column(IdDecorator, primary_key=True, index=True)
    name = Column(NameDecorator)
    description = Column(DescriptionDecorator)

    def __init__(self, id: Id, name: Name, description: Optional[Description]):
        self.id = id
        self.name = name
        self.description = description

    def get_id(self) -> Id:
        return self.id

    def get_name(self) -> Name:
        return self.name

    def get_description(self) -> Optional[Description]:
        return self.description
